import java.util.List;

/**
 * Created by hasee on 2017/9/25.
 */
public class ArrayList {
    public static void main(String[] args) {
        List<String>a = new java.util.ArrayList<String>();
        a.add(new String("测试1"));
        a.add(new String("测试2"));
        System.out.println(a);
        a.remove(new String("测试1"));
        System.out.println(a);
        if (!a.isEmpty()){
            System.out.println("有东西");
        }
        else
            System.out.println("没东西");
    }
}
