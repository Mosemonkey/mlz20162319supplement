package ch02;

/**
 * Created by hasee on 2017/6/14.
 */
public class TempConverter {
    public static void main(String[] args) {
        final int BASE = 32;
        final double CONVERSION_FACTOR = 9.0/5.0;

        double fahrenheitTemp;
        int celsiusTemp = 24;

        fahrenheitTemp = celsiusTemp*CONVERSION_FACTOR+BASE;

        System.out.println("Celsius Temperature:"+celsiusTemp);
        System.out.println("Fahrenheit Equivalent:"+fahrenheitTemp);
    }
}
