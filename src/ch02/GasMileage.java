package ch02;
import java.util.Scanner;
/**
 * Created by hasee on 2017/6/14.
 */
public class GasMileage {
    public static void main(String[] args) {
        int miles;
        double gallons,mpg;

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter the number of miles:");
        miles = scan.nextInt();

        System.out.print("Enter the gallons of fuel used:");
        gallons = scan.nextInt();

        mpg = miles/gallons;

        System.out.println("Miles per Gallon:"+mpg);
    }
}
