package ch07;
import java.text.NumberFormat;
/**
 * Created by hasee on 2017/6/17.
 */
public class CD {
    private String title,artist;
    private double cost;
    private int tracks;
    public CD(String name,String singer,double price,int numTracks){
        title=name;
        artist=singer;
        cost=price;
        tracks=numTracks;
    }
    public String toString()
    {NumberFormat fmt = NumberFormat.getPercentInstance();
    String description;
    description = fmt.format(cost)+"\t"+tracks+"\t";
    description += title+"\t"+artist;
    return description;

    }
}
