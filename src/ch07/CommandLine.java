package ch07;

/**
 * Created by hasee on 2017/6/18.
 */
public class CommandLine {
    public static void main(String[] args) {
        for (String arg : args)
            System.out.println(arg);
    }
}
