package ch07;

/**
 * Created by hasee on 2017/6/18.
 */
public class VariableParameters {
    public static void main(String[] args) {
        Family lewis = new Family("John","Sharon","Justin","Kayly","Nathan","Samantha");

        Family camden = new Family("Stephen","Annie","Matt","Marry","Simon","Lucy","Ruthie","Sam","David");

        System.out.println(lewis);
        System.out.println();
        System.out.println(camden);
    }
}
