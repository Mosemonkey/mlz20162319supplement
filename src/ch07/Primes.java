package ch07;

/**
 * Created by hasee on 2017/6/17.
 */
public class Primes {
    public static void main(String[] args) {
        int[]primeNums = {2,3,5,7,11,13,17,19};
        System.out.println("Array length:"+primeNums.length);
        System.out.println("The first few prime numbers are:");
        for (int prime:primeNums)
            System.out.print(prime+"  ");
    }
}
