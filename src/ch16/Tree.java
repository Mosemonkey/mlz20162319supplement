package ch16;

/**
 * Created by hasee on 2017/10/24.
 */
public class Tree {
    public static void main(String[] args) {
        LinkedBinaryTree L2 = new LinkedBinaryTree("D");
        LinkedBinaryTree R2 = new LinkedBinaryTree("F");
        LinkedBinaryTree L1 = new LinkedBinaryTree("B",L2,R2);
        LinkedBinaryTree L3 = new LinkedBinaryTree("G");
        LinkedBinaryTree R3 = new LinkedBinaryTree("E");
        LinkedBinaryTree R1 = new LinkedBinaryTree("C",L3,R3);
        LinkedBinaryTree T = new LinkedBinaryTree("A",L1,R1);

        T.postorder();
        T.preorder();
        T.inorder();
        System.out.println("后序"+T.postorder());
        System.out.println("中序"+T.inorder());
        System.out.println("前序"+T.preorder());

    }
}
