package ch16;

/**
 * Created by hasee on 2017/10/26.
 */
public class BackPainAnalyzer
{
    //-----------------------------------------------------------------

    //  Asks questions of the user to diagnose a medical problem.

    //-----------------------------------------------------------------

    public static void main (String[] args)
    {
        BackPainExpert expert = new BackPainExpert();
        expert.diagnose();
    }
}
