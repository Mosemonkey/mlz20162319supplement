package ch09;

/**
 * Created by hasee on 2017/6/19.
 */
public class Volunteer extends StaffMember {
    public Volunteer (String eName, String eAddress, String ePhone)
    {
        super (eName, eAddress, ePhone);
    }

    //-----------------------------------------------------------------

    //  Returns a zero pay value for this volunteer.

    //-----------------------------------------------------------------

    public double pay()
    {
        return 0.0;
    }
}
