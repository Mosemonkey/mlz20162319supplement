package cn.edu.besti.cs1623.M20162319;

import ch13.Searching;
import junit.framework.TestCase;

/**
 * Created by hasee on 2017/11/6.
 */
public class SearchingTest extends TestCase {
    Comparable[]list = new Comparable[5];
    ch13.Searching search =new Searching();
    public void testLinearSearch() throws Exception {
        list[0]= 4;
        list[1]= 5;
        list[2]= 16;
        list[3]= 2016;
        list[4]= 20162319;
        Comparable found = search.linearSearch(list,20162319);
        if (found==null)
        System.out.println("这里没有你需要的元素");
        else
            System.out.println("Found:"+found);
    }

    public void testBinarySearch() throws Exception {
        list[0]= 4;
        list[1]= 5;
        list[2]= 16;
        list[3]= 2016;
        list[4]= 20162319;
        Comparable found = search.binarySearch(list,20162319);
        if (found==null)
            System.out.println("这里没有你需要的元素");
        else
            System.out.println("Found:"+found);
    }

}