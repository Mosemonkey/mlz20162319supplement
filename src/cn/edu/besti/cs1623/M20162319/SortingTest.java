package cn.edu.besti.cs1623.M20162319;

import ch13.Sorting;
import junit.framework.TestCase;

/**
 * Created by hasee on 2017/11/6.
 */
public class SortingTest extends TestCase {
    ch13.Sorting sort = new Sorting();
    Comparable[]sun = {2319,23,19,2016,20162319};

    public void testSelectionSort() throws Exception {
        sort.selectionSort(sun);
        for (Comparable a:sun)
            System.out.println(a);
    }

    public void testInsertionSort() throws Exception {
        sort.insertionSort(sun);
        for (Comparable a:sun)
            System.out.println(a);
    }

    public void testBubblesort() throws Exception {
        sort.bubblesort(sun);
        for (Comparable a:sun)
            System.out.println(a);
    }

    public void testQuickSort() throws Exception {
        sort.quickSort(sun,0,sun.length-1);
        for (Comparable a:sun)
            System.out.println(a);
    }

    public void testMergeSort() throws Exception {
        sort.mergeSort(sun,0,sun.length-1);
        for (Comparable a:sun)
            System.out.println(a);
    }



}