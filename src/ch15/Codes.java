package ch15;

import java.util.LinkedList;

/**
 * Created by hasee on 2017/10/18.
 */


public class Codes
{

    public static void main ( String[] args)
    {
        int[] key = {5, 12, -3, 8, -9, 4, 10};
        Integer keyValue;
        String encoded = "", decoded = "";
        String message = "All programmers are playwrights and all " +
                "computers are lousy actors.";

        LinkedList<Integer> keyQueue1 = new LinkedList<Integer>();
        LinkedList<Integer> keyQueue2 = new LinkedList<Integer>();


        for (int scan=0; scan < key.length; scan++)
        {
            keyQueue1.add (new Integer(key[scan]));
            keyQueue2.add (new Integer(key[scan]));
        }


        for (int scan=0; scan < message.length(); scan++)
        {
            keyValue = keyQueue1.getFirst();
            encoded += (char) ((int)message.charAt(scan) + keyValue.intValue());
            keyQueue1.add (keyValue);
        }

        System.out.println ("Encoded Message:\n" + encoded + "\n");


        for (int scan=0; scan < encoded.length(); scan++)
        {
            keyValue = keyQueue2.getFirst();
            decoded += (char) ((int)encoded.charAt(scan) - keyValue.intValue());
            keyQueue2.add (keyValue);
        }

        System.out.println ("Decoded Message:\n" + decoded);
    }
}
