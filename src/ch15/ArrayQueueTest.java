package ch15;

import junit.framework.TestCase;

/**
 * Created by hasee on 2017/10/18.
 */
public class ArrayQueueTest extends TestCase{

    CircularArrayQueue sun = new CircularArrayQueue();
    public void testisEmpty()throws Exception{
        assertEquals(true,sun.isEmpty());
        sun.enqueue(1);
        assertEquals(false,sun.isEmpty());

    }
    public void testsize()throws Exception{
        assertEquals(0, sun.size());
        sun.enqueue(1);
        assertEquals(1, sun.size());
    }
    public void testfirst()throws Exception{
        sun.enqueue(1);
        assertEquals(1, sun.first());
    }
    public void testdequeue()throws Exception{
        sun.enqueue(1);
        assertEquals(1, sun.dequeue());
    }
    public void testtoString() throws Exception{
        sun.enqueue("莫礼钟");
        assertEquals("莫礼钟 ",sun.toString());
    }


}
