package ch15;

import junit.framework.TestCase;

/**
 * Created by hasee on 2017/10/19.
 */
public class LinkedQueuetest extends TestCase {
    LinkedQueue sun = new LinkedQueue();
    public void testsize()throws EmptyStackException{
        assertEquals(0,sun.size());
        sun.enqueue(1);
        assertEquals(1,sun.size());
    }
    public void testisEmpty()throws EmptyStackException{
        assertEquals(true,sun.isEmpty());
        sun.enqueue(1);
        assertEquals(false,sun.isEmpty());
    }
    public void testtoString()throws  EmptyStackException{
        sun.enqueue("莫礼钟");
        assertEquals("莫礼钟",sun.toString());
    }
    public void testfirst()throws EmptyStackException{
        sun.enqueue(1);
        assertEquals(1,sun.first());
    }
    public void testdequeue()throws EmptyStackException{
        sun.enqueue(1);
        assertEquals(1,sun.dequeue());
    }
}
