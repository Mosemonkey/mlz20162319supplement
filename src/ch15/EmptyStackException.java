package ch15;

/**
 * Created by hasee on 2017/10/19.
 */
public class EmptyStackException extends RuntimeException{
    public EmptyStackException(String str){
        super("Error"+ str);
    }
}
