package ch15;

/**
 * Created by hasee on 2017/10/18.
 */
public interface Queue<T> {
    public void enqueue (T element);


    public T dequeue();


    public T first();


    public boolean isEmpty();

    public int size();


    public String toString();
}
