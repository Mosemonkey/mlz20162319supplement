package ch08;

/**
 * Created by hasee on 2017/6/19.
 */
public class Book {
    protected int pages = 1500;
    public void setPages (int numPages)
    {
        pages = numPages;
    }
    public int getPages ()
    {
        return pages;
    }
}
