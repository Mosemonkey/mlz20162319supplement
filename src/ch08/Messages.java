package ch08;

/**
 * Created by hasee on 2017/6/19.
 */
public class Messages {
    public static void main (String[] args)
    {
        Thought parked = new Thought();
        Advice dates = new Advice();

        parked.message();

        dates.message();

    }
}
