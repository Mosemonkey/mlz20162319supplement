package ch08;

/**
 * Created by hasee on 2017/6/19.
 */
public class Advice extends Thought{
    public void message()
    {
        System.out.println ("Warning: Dates in calendar are closer " +
                "than they appear.");

        System.out.println();

        super.message();

    }
}
