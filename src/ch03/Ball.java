package ch03;
import java.util.Scanner;
import java.text.DecimalFormat;
/**
 * Created by hasee on 2017/6/17.
 */
public class Ball {
    public static void main(String[] args) {
        double a,b,c;

        Scanner scan = new Scanner(System.in);
        System.out.println("输入球的半径：");
        a = scan.nextDouble();
        b = (double)3*Math.PI*Math.pow(a,3)/4;
        c = 4*Math.PI*Math.pow(a,2);
        DecimalFormat fmt = new DecimalFormat("0.####");
        System.out.println("球的体积是："+fmt.format(b));
        System.out.println("球的表面积是："+fmt.format(c));
    }
}
