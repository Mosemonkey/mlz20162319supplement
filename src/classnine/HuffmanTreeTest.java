package classnine;

public class HuffmanTreeTest
{
    public static void main(String[] args)
    {
        String text="AAAABBBCDDBBAAA";                     //【例6.4】 数据
        int[] weight6_28={7,5,1,2};                        //图6.26指定权值集合，默认字符集为"ABCD"
        HuffmanTree huffman = new HuffmanTree(weight6_28); //构造Huffman树
        System.out.println(huffman.toString());            //输出Huffman树的结点数组和所有字符编码
        String compressed = huffman.encode(text);
        System.out.println("将"+text+"压缩为"+compressed+"，"+compressed.length()+"位");
        System.out.println("将"+compressed+"解码为"+huffman.decode(compressed));


        int[] weight6_34={5,29,7,8,14,23,3,11};            //图6.34指定权值集合，默认字符集为"ABCDEFGH"
        huffman = new HuffmanTree(weight6_34);             //构造Huffman树
        System.out.println(huffman.toString());            //输出Huffman树的结点数组和所有字符编码
    }
}
