package ch04;
import java.util.Scanner;
/**
 * Created by hasee on 2017/6/16.
 */
public class PalindromeTester {
    public static void main(String[] args) {
        String str,another = "y";
        int left,right;

        Scanner scan = new Scanner(System.in);
        while (another.equalsIgnoreCase("y")){
            System.out.println("Enter a potential palindrome:");
            str = scan.nextLine();

            left = 0;
            right = str.length()-1;

            while (str.charAt(left) == str.charAt(right)&&left<right){
                left ++;
                right --;
            }
            if (left<right)
            System.out.println("That string is NOT a palindrome.");
            else
                System.out.println("That string IS a palindrome.");
            System.out.println();
            System.out.print("Test another palindrome (y/n)？");
            another = scan.nextLine();
        }
    }
}
