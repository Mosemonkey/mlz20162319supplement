package ch04;

/**
 * Created by hasee on 2017/6/16.
 */
public class Stars {
    public static void main(String[] args) {
        final int MAX_ROWS = 10;
        for (int row = 1; row <= MAX_ROWS; row++){
            for (int star = 1; star <= row ; star++)
                System.out.println("*");

            System.out.println();
        }
    }
}
