package Thefourthdexperiment;

/**
 * Created by hasee on 2017/11/20.
 */
public class TestLinkedlist {
    public static void main(String[] args)
    {
        Linkedlist objList = new Linkedlist();

        Boolean b = new Boolean(true);
        Character c = new Character('$');
        Integer i = new Integer(34567);
        String s = new String("Hello");

        objList.insertAtFront(b);
        objList.print();
        objList.insertAtFront(c);
        objList.print();
        objList.insertAtBack(i);
        objList.print();
        objList.insertAtBack(s);
        objList.print();

        Object removedObj;

        removedObj = objList.removeFromFront();
        System.out.println(removedObj.toString() + " 鏈錶已經被移除。");
        objList.print();
        removedObj = objList.removeFromFront();
        System.out.println(removedObj.toString() + " 鏈錶已經被移除");
        objList.print();
        removedObj = objList.removeFromBack();
        System.out.println(removedObj.toString() + " 鏈錶已經被移除");
        objList.print();
        removedObj = objList.removeFromBack();
        System.out.println(removedObj.toString() + " 鏈錶已經被移除");
        objList.print();
    }
}
