package Thefourthdexperiment;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
/**
 * Created by hasee on 2017/11/27.
 */
public class RouterService extends MatrixUDG{

    int Dist[] = new int[mVexs.length];
    int Count;
    int S[] = new int[mVexs.length];

    public int sort(int a, int b){
            int u = 0;
            for (int i = 0; i<count;i++)
                if (mMatrix[a][i]==0)
                    Dist[i]=999999999;
                else Dist[i]=mMatrix[a][i];
            while (Count != count) {
                for (int i = 0; i < count; i++)
                    if (Dist[i] < Dist[u])
                        u = i;
                S[Count++] = Dist[u];
                for (int x = 0; x < count; x++)
                    if (Dist[x] > Dist[u] + mMatrix[u][x])
                        Dist[x] = Dist[u] + mMatrix[u][x];
            }
            return Dist[b];
        }
}
