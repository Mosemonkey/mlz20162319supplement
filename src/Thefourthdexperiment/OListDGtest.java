package Thefourthdexperiment;

/**
 * Created by hasee on 2017/11/27.
 */
public class OListDGtest {
    public static void main(String args[]) {
        // 顶点数组
        char[] vexs = {
                'A', 'B', 'C', 'D'
        };
        // 边数组
        char[][] edges = new char[][] {
                {
                        'A', 'B'
                }, {
                'A', 'C'
        }, {
                'A', 'D'
        }, {
                'B', 'D'
        }, {
                'C', 'D'
        }
        };

        OListDG listUDG = new OListDG(vexs, edges);
        listUDG.print();
    }
}
