package ch18;

import ch16.BinaryTree;

/**
 * Created by hasee on 2017/11/3.
 */
public interface MaxHeap<T extends Comparable<T>> extends BinaryTree<T>
{
    //  Adds the specified object to the heap.
    public void add (T obj);

    //  Returns a reference to the element with the highest value in
    //  the heap.
    public T getMax ();

    //  Removes and returns the element with the highest value in the
    //  heap.
    public T removeMax ();
}
