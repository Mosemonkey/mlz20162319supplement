package ch18;

/**
 * Created by hasee on 2017/10/22.
 */
public class EmptyCollectionException extends RuntimeException{
    public EmptyCollectionException(String str){
        super("Error"+ str);
    }
}
